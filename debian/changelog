python-django-tinymce (2.7.0-1) UNRELEASED; urgency=medium

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Ondřej Nový ]
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/control: Deprecating priority extra as per policy 4.0.1
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.1.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Jochen Sprickerhof ]
  * Update watch file with new location
  * New upstream version 2.7.0

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 28 Dec 2018 18:54:08 +0100

python-django-tinymce (1.5-3) unstable; urgency=low

  [ Jakub Wilk ]
  * Switch to source format 3.0 (quilt).
  * Fix watch file.
  * Add patch to fix import error with Django 1.2 (closes: #587073). Thanks to
    Ionut Ciocirlan for the bug report.
  * Bump standards version to 3.9.0 (no changes needed).

 -- Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>  Tue, 06 Jul 2010 22:11:05 +0200

python-django-tinymce (1.5-2) unstable; urgency=low

  * Change priority from 'optional' to 'extra', as the package conflicts with
    python-tinymce which is 'optional', violating Policy Section 2.5.
  * Add Debian Python Modules Team to Uploaders and update Vcs-* fields to
    reflect the move to DPMT version control.
  * Bumped Standards-Version.

 -- Daniel Watkins <daniel@daniel-watkins.co.uk>  Wed, 13 May 2009 14:17:32 +0100

python-django-tinymce (1.5-1) unstable; urgency=low

  * Initial release. (Closes: #516310)

 -- Daniel Watkins <daniel@daniel-watkins.co.uk>  Tue, 24 Feb 2009 18:15:15 +0000
